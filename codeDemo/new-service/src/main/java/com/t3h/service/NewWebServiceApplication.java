package com.t3h.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewWebServiceApplication.class, args);
    }

}
