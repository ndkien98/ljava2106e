package com.t3h.news.model.entity;

public class Category {

    private int id;
    private String name;
    private int parentsCategoryId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParentsCategoryId() {
        return parentsCategoryId;
    }

    public void setParentsCategoryId(int parentsCategoryId) {
        this.parentsCategoryId = parentsCategoryId;
    }
}
