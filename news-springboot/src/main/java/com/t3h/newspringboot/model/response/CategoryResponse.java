package com.t3h.newspringboot.model.response;

import lombok.Data;

import javax.persistence.Column;

@Data
public class CategoryResponse {

    private Integer id;
    private String name;

    private Integer parentsCategoryId;

}
