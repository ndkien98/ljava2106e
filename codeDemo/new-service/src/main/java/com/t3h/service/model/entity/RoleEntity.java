package com.t3h.service.model.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name = "ROLE")
@Entity
public class RoleEntity extends BaseEntity{

    private int name;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name = "ROLE_USER",
    joinColumns = {@JoinColumn(name = "role_id")},
    inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<UserEntity> setUser = new HashSet<>();
}
