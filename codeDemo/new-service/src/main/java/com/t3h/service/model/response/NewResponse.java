package com.t3h.service.model.response;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class NewResponse {

    private Integer id;

    private Timestamp createDate;

    private Timestamp updateDate;

    private String categoryName;

    private int editorId;

    private String title;

    private String content;

    private String avatar;

    private String author;

    private String originalResource;

    private int numberAccess;

    private String censorName;

    private String creatorName;
}
