package com.t3h.newspringboot.model.response;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class NewResponse {

    private Integer id;

    private Timestamp createDate;

    private Timestamp updateDate;

    private Integer creatorId;

    private Integer editorId;

    private String title;

    private String content;

    private String avatar;

    private String author;

    private Integer categoryId;

    private String originalResource;

    private Integer numberAccess;

    private String censorName;
}
