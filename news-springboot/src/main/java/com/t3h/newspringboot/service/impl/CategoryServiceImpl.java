package com.t3h.newspringboot.service.impl;

import com.t3h.newspringboot.model.response.CategoryResponse;
import com.t3h.newspringboot.repository.CategoryRepository;
import com.t3h.newspringboot.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {


    private final CategoryRepository categoryRepository;
    private final ModelMapper modelMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
    }

    public List<CategoryResponse> getList(){
        return categoryRepository.findAll()
                .stream().map(data -> modelMapper.map(data,CategoryResponse.class)).collect(Collectors.toList());
    }


}
