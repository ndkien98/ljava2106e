package com.t3h.newspringboot.service.security;

import com.t3h.newspringboot.model.entity.RoleEntity;
import com.t3h.newspringboot.model.entity.UserEntity;
import com.t3h.newspringboot.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity userEntity = userRepository.findByUsername(username);
        if (userEntity == null){
            throw new UsernameNotFoundException("user not fount");
        }

//        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//        Set<RoleEntity> roleEntities = userEntity.getSetRole();
//        for (RoleEntity role:roleEntities
//             ) {
//            SimpleGrantedAuthority simpleGrantedAuthority =
//                    new SimpleGrantedAuthority(role.getName());
//            grantedAuthorities.add(simpleGrantedAuthority);
//        }
//
//        UserDetails userDetails = new User(userEntity.getUsername(),userEntity.getPassword(),grantedAuthorities);
        return new CustomUserDetails(userEntity);
    }
}
