package com.t3h.newspringboot.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name = "USER")
@Entity
public class UserEntity extends BaseEntity{

    private String username;

    private String password;

    private String fullName;

    private String address;

    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_user",
    joinColumns = {@JoinColumn(name = "user_id")},
    inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<RoleEntity> setRole = new HashSet<>();


    /**
     * 1 user co the dang nhieu ban tin (news)
     * 1 ban thin chi duoc dang boi 1 user
     * 1 - n
     * FetchType.LAZY: neu lấy ra userEntity sẽ không lấy ra danh sách news
     *              , chỉ lấy ra danh sách news khi gọi tới setNews
     *FetchType.EAGER: lay ra toàn bộ NewsEntity khi lấy ra userentity trong database
     * cascade : quản lý việc CRUD news sẽ ảnh hưởng tới setNews như thế nào
     */
    @OneToMany(fetch = FetchType.LAZY,mappedBy = "censor")
    private Set<NewsEntity> setNews = new HashSet<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<RoleEntity> getSetRole() {
        return setRole;
    }

    public void setSetRole(Set<RoleEntity> setRole) {
        this.setRole = setRole;
    }

    public Set<NewsEntity> getSetNews() {
        return setNews;
    }

    public void setSetNews(Set<NewsEntity> setNews) {
        this.setNews = setNews;
    }
}
