package com.t3h.newspringboot.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name = "ROLE")
@Entity
public class RoleEntity extends BaseEntity{

    private String name;

    @ManyToMany(mappedBy = "setRole",fetch = FetchType.LAZY)
    private Set<UserEntity> setUser = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserEntity> getSetUser() {
        return setUser;
    }

    public void setSetUser(Set<UserEntity> setUser) {
        this.setUser = setUser;
    }
}
