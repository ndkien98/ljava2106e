
create database customer_manager;

use customer_manager;

create table customer(
    id integer primary key ,
    fullName nvarchar(100) ,
    address nvarchar (100),
    phone varchar(11),
    email varchar(50)
);

create table id_card (
        id integer primary key auto_increment,
        numberIdCard varchar(30)
);

alter table customer add column idCard integer unique;
alter table customer add foreign key (idCard) references customer_manager.id_card(id);
alter table id_card add column idCustomer integer unique ;

select id_card.idCustomer from id_card;

alter table id_card add constraint
    fk_customer foreign key (idCustomer) references customer_manager.customer(id);

alter table id_card drop foreign key fk_customer;


create table position (
    id integer primary key auto_increment,
    name nvarchar(30)
);

alter table customer add column idPosition integer ;

use customer_manager;
alter table customer add constraint foreign key (idPosition) references customer_manager.position(id);

select * from position;

select * from customer;

insert into customer (id, fullName, address, phone, email)
VALUE (2,'Nguy?n �?c Minh','H� N?i','0925334772','luyen@gmail.com');
insert into customer (id, fullName, address, phone, email)
    VALUE (3,'Nguy?n Tu?n Anh','H� N?i','0825334772','luyen@gmail.com');
insert into customer (id, fullName, address, phone, email)
    VALUE (4,'L� Thi�n Tu?n','H� N?i','0725334772','luyen@gmail.com');
insert into customer (id, fullName, address, phone, email)
    VALUE (5,'Nguy?n Van Thanh','H� N?i','0325334772','luyen@gmail.com');

-- Update barn ghi c� id = 3 th�nh d?a ch? H? ch� minh

update customer set address = 'H? Ch� Minh' where id = 3;

select * from customer where id = 3 or id = 5;

select * from customer where id >2 and address = 'H� N?i';

select name, from position inner join
    customer on position.id=customer.idPosition
where customer.fullName = 'L� Van Luy?n';

select * from customer order by id desc ;

-- 1-n , 1 lop se co nhieu sinh vien,
create table class(
    id integer primary key ,
    name varchar(100)
);

create table student(
                      id integer primary key ,
                      name varchar(100),
                      idClass integer
);
alter table student add foreign key (idClass) references class(id);
create table teacher(
                        id integer primary key ,
                        name nvarchar(100)
);
create table teacher_student(
       id integer primary key ,
       idStudent integer ,
       idTeacher integer
);

alter table teacher_student add foreign key (idStudent) references customer_manager.student(id);
alter table teacher_student add foreign key (idTeacher) references customer_manager.teacher(id);
-- n - n ,
-- 1 L?y ra danh s�ch c�c gi?ng vi�n d� t?ng d?y student b
-- 2 L?y ra d�nh sach c�c sinh vi�n d� t?ng du?c teacher b d?y

select * from student;
select * from teacher;
select * from teacher_student;

select * from teacher where id in (
    select teacher_student.idTeacher from teacher_student where idStudent = (
        select student.id from student where student.name = 'student b')
    );

select * from teacher inner join
    teacher_student inner join
    student on teacher.id = teacher_student.idTeacher
                   and teacher_student.idStudent = student.id
where student.name = 'student b';

select * from teacher_student inner join teacher inner join
    student on teacher_student.idTeacher = teacher.id and teacher_student.idStudent = student.id
where student.name = 'student b';

-- 1
select * from student inner join teacher_student inner join teacher  on teacher.id = teacher_student.idTeacher
    and teacher_student.idStudent = student.id where teacher.name = 'teacher a';






