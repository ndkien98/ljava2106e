package com.t3h.news.dao.impl;

import com.t3h.news.dao.INewsDao;
import com.t3h.news.model.entity.News;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class NewsDaoImpl implements INewsDao {

    // lay ra danh sach cac bai news trong co so du lieu

        // 1 : tao connect den co so du lieu

        // 2: tao ra cau lenh select sql

        // 3: thuc thi cau lenh vao co so du lieu

        // 4: map du lieu tu table -> class object

    // 1 : tao connect den co so du lieu
    public Connection getConnection (){
        String url = "jdbc:mysql://localhost:3306/24h";
        String username = "root";
        String password = "1234";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(url,username,password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public List<News> getNews(){

        Connection connection = this.getConnection();
        String sql = "select * from news";

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        List<News> listNews = new ArrayList<>();
        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                News news = new News();
                news.setId(Integer.parseInt(resultSet.getString("id")));
                news.setTitle(resultSet.getString("title"));
                news.setAvatar(resultSet.getString("avatar"));
                news.setCreatorId(Integer.parseInt(resultSet.getString("creatorId")));
                news.setAuthor(resultSet.getString("author"));
                news.setEditorId(Integer.parseInt(resultSet.getString("editorId")));
                listNews.add(news);
            }
            return listNews;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
