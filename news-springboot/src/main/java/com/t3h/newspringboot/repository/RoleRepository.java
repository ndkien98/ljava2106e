package com.t3h.newspringboot.repository;

import com.t3h.newspringboot.model.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoleRepository extends JpaRepository<RoleEntity,Integer> {


}
