package com.t3h.service.model.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "NEW")
@Entity
public class NewsEntity extends BaseEntity{

    private String title;

    @Column(columnDefinition = "text")
    private String content;

    private String avatar;

    private String author;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    private CategoryEntity category;

    private String originalResource;

    private Integer numberAccess;

    @ManyToOne
    @JoinColumn(name = "censor")
    private UserEntity censorUser;


}
