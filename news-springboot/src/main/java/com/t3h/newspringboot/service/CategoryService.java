package com.t3h.newspringboot.service;

import com.t3h.newspringboot.model.response.CategoryResponse;

import java.util.List;

public interface CategoryService {

    List<CategoryResponse> getList();
}
