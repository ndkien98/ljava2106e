package com.t3h.newspringboot.model.entity;

import lombok.Data;

import javax.persistence.*;

@Table(name = "NEWS")
@Entity
/**
 * ORM non relationship
 */
public class NewsEntity extends BaseEntity{

    private String title;

    @Column(columnDefinition = "text")
    private String content;

    private String avatar;

    private String author;

    @Column
    private Integer categoryId;

    private String originalResource;

    private Integer numberAccess;

    @ManyToOne
    @JoinColumn(name = "censor")
    private UserEntity censor;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getOriginalResource() {
        return originalResource;
    }

    public void setOriginalResource(String originalResource) {
        this.originalResource = originalResource;
    }

    public Integer getNumberAccess() {
        return numberAccess;
    }

    public void setNumberAccess(Integer numberAccess) {
        this.numberAccess = numberAccess;
    }

    public UserEntity getCensor() {
        return censor;
    }

    public void setCensor(UserEntity censor) {
        this.censor = censor;
    }
}
