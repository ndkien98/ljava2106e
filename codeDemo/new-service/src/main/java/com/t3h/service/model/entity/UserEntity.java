package com.t3h.service.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name = "USER")
@Entity
public class UserEntity extends BaseEntity{

    private String username;

    private String password;

    private String fullName;

    private String address;

    private String email;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinTable(name = "ROLE_USER",
    joinColumns = {@JoinColumn(name = "user_id")},
    inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<RoleEntity> setRole = new HashSet<>();


    @OneToMany(fetch = FetchType.LAZY,mappedBy = "censorUser")
    private Set<NewsEntity> setNewsEntity = new HashSet<>();
}
