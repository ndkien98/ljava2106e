package com.t3h.news.model.entity;

import lombok.Data;

@Data
public class User extends BaseEntity{

    private String username;
    private String password;
    private String fullName;
    private String address;
    private String email;


}
