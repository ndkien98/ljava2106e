package com.t3h.service.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name = "CATEGORY")
@Entity
public class CategoryEntity extends BaseEntity{

    private String name;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "category")
    private Set<NewsEntity> setNews = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "categoryParent")
    private Set<CategoryEntity> setCategory = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name = "categoryParentId")
    private CategoryEntity categoryParent;
}
